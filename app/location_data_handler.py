from tornado.web import RequestHandler
from http import HTTPStatus
import json
from app.location_data import location_data
#import location_data

class LocationHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieve information about a city.
        :param args:
        :param kwargs:
        :return:
        """
                
        location_data_inst = location_data(args[0])
        
        try:
            location_data_inst.get_csv_data()
        except:
            self.set_status(HTTPStatus.BAD_REQUEST)
            return

        try:
            location_data_inst.get_city_info()
            response = {'city_name': args[0].lower(),
                        'current_temperature': location_data_inst.get_current_temp(),
                        'current_weather_description': location_data_inst.get_current_weather_description(),
                        'population': location_data_inst.get_population(),
                        'bars': location_data_inst.get_num_bars(),
                        'city_score': location_data_inst.get_city_score()}
            self.set_status(HTTPStatus.OK)
            self.write(json.dumps(response))
            return
        except:
            self.set_status(HTTPStatus.NOT_FOUND)
            return




