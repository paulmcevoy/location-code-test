from tornado.web import RequestHandler
from http import HTTPStatus
import json
from app.location_data import location_data


class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for each city.
        :param args:
        :param kwargs:
        :return:
        """
        city_list = [item for item in args[0].split(",")]
        response = {}
        response['city_data'] = []
        for city in city_list:
            location_data_inst = location_data(city)
            
            try:
                location_data_inst.get_csv_data()
            except:    
                print("No city found {}".format(city))
                self.set_status(HTTPStatus.BAD_REQUEST)

            try:
                location_data_inst.get_city_info()
            except:
                self.set_status(HTTPStatus.BAD_REQUEST)
                return
            city_score = location_data_inst.get_city_score()
            city_dict = {'city_name': city, 'city_rank': None, 'city_score': city_score}
            response['city_data'].append(city_dict)

        newlist = sorted(response['city_data'], key=lambda k: k['city_score'],reverse=True)
        for i in range(len(newlist)):
            newlist[i]['city_rank'] = i+1
        response['city_data'] = newlist

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
