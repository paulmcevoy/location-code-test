'''
Author: Paul McEvoy

This file calls the following APIs:
Dark Weather
MetaWeather
Distance24
GEOIP

It also reads the CSV located at:
../data/european_cities.csv

It obtains the following information:

API:
A text description of the current weather
The current temperature
The min and max temperatures for the next 5 days from two sources
The location of the user (based on the IP)
The distance to the requested city from the user's location

From the CSV:
The population of the city
The number of bars in the city
The quality of the public transport in the city
The average hotel cost
The crime rate

From this it calculates:
The bar density (the number of bars per population)
The average temperature for the next 5 days between the two weather sources

To calculate the total score:
Bar density and transport values are scored proportionally
Crime and hotel values are scored inversely proportionally

An IDEAL_TEMP is compared against the destination city average temperature
The difference between the dest temp and ideal are scored with decreasing score
with increased difference
The total score is the mean of all individual scores

Conforms with pep8
'''

from http import HTTPStatus
import requests
import numpy as np
import logging
import sys
import pandas as pd
import os

logging.basicConfig(level=logging.INFO)
# silence URL logs
logging.getLogger("urllib3").setLevel(logging.WARNING)

DARK_URL = 'https://api.forecast.io/forecast/90190dd79db32a4b5dac71a8b67facef/'
META_URL = 'https://www.metaweather.com/api/location/search/?query='
GEOI_URL = 'http://freegeoip.net/json'
DIST_URL = 'http://www.distance24.org/route.json?stops='


def get_json(url):
    try:
        response = requests.get(url)
        return response.json()
    except requests.exceptions.RequestException as e:
        logging.error("Error: No valid response, bad URL? {} ".format(e))
        sys.exit(1)


def get_averages(weather_list, temp_string_max, temp_string_min):
    temp_list = []
    for each_day in weather_list:
        temp_list.append(each_day[temp_string_max])
        temp_list.append(each_day[temp_string_min])
    return np.mean(temp_list)


def get_meta_temp(cityname):
    url = META_URL + cityname
    loc_json = get_json(url)
    woeid = loc_json[0]['woeid']
    weather = get_json('https://www.metaweather.com/api/location/' + str(woeid))
    data = weather['consolidated_weather']
    get_averages(data, 'max_temp', 'min_temp')
    return data[0]['weather_state_abbr'],\
        loc_json[0]['latt_long'].split(',')[0],\
        loc_json[0]['latt_long'].split(',')[1],\
        get_averages(data, 'max_temp', 'min_temp')


def get_dark_temp(lat, long):
    url = DARK_URL + str(lat) + ',' + str(long) + '?&units=si'
    loc_json = get_json(url)
    data = loc_json['daily']['data']
    current_temp = loc_json['currently']['apparentTemperature']
    get_averages(data, 'temperatureMax', 'temperatureMin')
    return data[0]['summary'],\
        get_averages(data, 'temperatureMax', 'temperatureMin'),\
        current_temp


def get_geo():
    loc_json = get_json(GEOI_URL)
    return loc_json['city']


def get_dist(city1, city2):
    url = DIST_URL + city1 + '|' + city2
    loc_json = get_json(url)
    distance = loc_json['distance']
    return distance


class location_data(object):

    IDEAL_TEMP = 20
    IDEAL_DIST = 800
    MAX_DIST = 3000
    DIST_RANGE = MAX_DIST - IDEAL_DIST

    def __init__(self, city):

        self.city = city.lower()
        # Ensure relative path
        dir = os.path.dirname(__file__)
        filename = os.path.join(dir, '../data/european_cities.csv')
        self.df_european_cities = pd.read_csv(filename)

        # There are a few corrupted rows in the CSV with '?' symbol. Remove them.
        self.df_european_cities = self.df_european_cities[self.df_european_cities.city.str.contains("\?") == False]
        self.df_european_cities['city'] = self.df_european_cities['city'].str.lower()

    def get_csv_data(self):

        self.df_european_cities.set_index('city', inplace=True)
        self.df_european_cities['bar_dens'] = self.df_european_cities['bars'] / self.df_european_cities['population']
        self.num_bars = int(self.df_european_cities.loc[[self.city], 'bars'])
        self.min_bars_density = self.df_european_cities['bar_dens'].min()
        self.bars_density_range = self.df_european_cities['bar_dens'].max() - self.df_european_cities['bar_dens'].min()
        self.average_hotel_cost_range = self.df_european_cities['average_hotel_cost'].max() - self.df_european_cities['average_hotel_cost'].min()
        self.average_hotel_cost_min = self.df_european_cities['average_hotel_cost'].min()
        self.average_hotel_cost = int(self.df_european_cities.loc[[self.city], 'average_hotel_cost'])
        self.transport = int(self.df_european_cities.loc[[self.city], 'public_transport'])
        self.population = int(self.df_european_cities.loc[[self.city], 'population'])
        self.crime = int(self.df_european_cities.loc[[self.city], 'crime_rate'])
        self.bars_density = self.num_bars / self.population
        return True

    def get_city_info(self):
        self.my_city = get_geo()
        self.weather_code, latt, long, meta_temp = get_meta_temp(self.city)
        self.weather_description, dark_temp, self.current_temp = get_dark_temp(latt, long)
        self.average_temp = (meta_temp + dark_temp) / 2.0
        self.distance = get_dist(self.my_city, self.city)
        return True

    def get_num_bars(self):
        return self.num_bars

    def get_population(self):
        return self.population

    def get_current_temp(self):
        return self.current_temp

    def get_ave_temp(self):
        return self.average_temp

    def get_current_weather_description(self):
        return self.weather_description

    def get_weather_code(self):
        return self.weather_code

    def get_distance(self):
        return self.distance

    def get_city_score(self):

        # An ideal distance is selected below which any distance lower will get 1 for the score
        # Greater than a max distance will get a score of 0
        # Anything in between gets as score between 0 and 1

        if self.distance < self.IDEAL_DIST:
            distance_score = 1
        elif self.distance > self.MAX_DIST:
            distance_score = 0
        else:
            distance_score = (self.distance - self.IDEAL_DIST) / self.DIST_RANGE

        # For temperature we get normalised score that represents the distance from our IDEAL_TEMP
        temp_score = 1 - ((abs(self.average_temp - self.IDEAL_TEMP)) / 30)
        bar_score = (self.bars_density - self.min_bars_density) / self.bars_density_range
        transport_score = self.transport / 10
        crime_score = 1 - (self.crime / 10)
        hotel_score = 1 - ((abs(self.average_hotel_cost - self.average_hotel_cost_min)) / self.average_hotel_cost_range)

        total_score = np.mean([temp_score, bar_score, transport_score, crime_score, hotel_score, distance_score])
        return round(total_score, 3)
